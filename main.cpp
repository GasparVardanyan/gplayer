# include "MainWindow.hpp"

# include <QApplication>

// # include <system>

// # include "PlayerManager.hpp"
# include "PlaylistParser.hpp"
# include "RegularPlaylistProvider.hpp"

int main (int argc, char * argv [])
{
	// QApplication a (argc, argv);
	// MainWindow w;
	// w.show();

	// PlayerManager test;


	QSharedPointer <Playlist> playlist =
		QSharedPointer <Playlist> ::create (
			QSharedPointer <RegularPlaylistProvider>::create ("/media/music/music")
		)
	;

	PlaylistParser::Parse (playlist);

	// return a.exec ();
}
