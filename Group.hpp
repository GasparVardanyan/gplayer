# pragma once

# include "PlaylistItem.hpp"

class Art;
class Track;

class Group : public PlaylistItem
{
	friend class PlaylistParser;

public:
	Group (const QString & source);

	virtual QList <QSharedPointer <Track>> tracks () override final;

	QSharedPointer <Art> art () const;

protected:
	void setArt (QSharedPointer <Art> art);
	void addTrack (QSharedPointer <Track> track);

private:
	QSharedPointer <Art>  m_art;
	QList <QSharedPointer <Track>> m_tracks;
};
