# pragma once

# include <QSharedPointer>

class QString;
class QIODevice;

class PlaylistParser;

enum class PlaylistProviderStatus : int {
	Ok
	, NotOpened
	, FailedToOpen
};

class IPlaylistProvider
{
public:
	IPlaylistProvider ();
	virtual ~IPlaylistProvider ();

	virtual void openDirectory (const QString & path) =0;
	virtual bool isReadOnly () const =0;
	virtual PlaylistProviderStatus status () const =0;

	virtual QSharedPointer <QIODevice> openFile (const QString & path) const =0;
	virtual bool fileExists (const QString & path) const =0;

private:
	PlaylistParser * m_playlistParser = nullptr;
};
