# pragma once

# include <QString>

# include "Playlist.hpp"

class QRegularExpression;

class IPlaylistProvider;

class PlaylistParser
{
	struct TrackInfo;
	struct GroupInfo;

	static const inline QString File_Playlist =
		"pl_list.m3u"
	;

	static const QRegularExpression
		  Matcher_Blank
		, Matcher_Comment
		, Matcher_M3U_Header
		, Matcher_M3U_Group
		, Matcher_M3U_Info
		, Matcher_M3U_Playlist_Title
		, Matcher_Art_Separator
		, Matcher_File_M3U
		, Matcher_URL_Youtube
	;

public:
	PlaylistParser () =delete;

	static void Parse (QSharedPointer <Playlist> playlist);

private:
	static GroupInfo ReadGroup (
		  QSharedPointer <IPlaylistProvider> provider
		, const QString & m3uFile
		, PlaylistStatus & status
	);
};
