# pragma once

# include "IPlaylistProvider.hpp"

class QString;

class RegularPlaylistProvider : public IPlaylistProvider
{
public:
	RegularPlaylistProvider (const QString & path);

	virtual void openDirectory (const QString & path) override final;
	virtual bool isReadOnly () const override final;
	virtual PlaylistProviderStatus status () const override final;
	virtual QSharedPointer <QIODevice> openFile (const QString & path) const override final;
	virtual bool fileExists (const QString & path) const override final;

private:
	QString m_rootDir;
};
