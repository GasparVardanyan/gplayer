# include "Group.hpp"

Group::Group (const QString & source)
	: PlaylistItem (PlaylistItem::Type::Group, source)
{

}

void Group::setArt (QSharedPointer <Art> art)
{
	m_art = art;
}

QSharedPointer <Art> Group::art () const
{
	return m_art;
}

QList <QSharedPointer <Track>> Group::tracks ()
{
	return m_tracks;
}

void Group::addTrack (QSharedPointer <Track> track)
{
	m_tracks.append (track);
}
