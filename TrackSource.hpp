# pragma once

# include <QString>
# include <QList>
# include <QSharedPointer>

class IPlaylistProvider;

class TrackSource
{
public:
	enum class Quality : int {
		  Default
		, FullHD
		, AudioOnly
	};

	enum class MediaType : int {
		  Unknown
		, Audio
		, Video
	};

public:
	static const inline QList <QString> AudioFileExtensions = {
		"opus", "m4a"
	};

	static const inline QList <QString> VideoFileExtensions = {
		"mkv", "mp4"
	};

	static const inline QList <QString> ThumbnailExtensions = {
		"jpg"
	};

	static const inline QString ThumbnailsDirectory =
		".thumbnails"
	;

public:
	TrackSource (Quality quality);
	Quality quality () const;
	MediaType mediaType () const;

	// static QSharedPointer <TrackSource> findByYoutubeKey (
	// 	  const QString & id
	// 	, QSharedPointer <IPlaylistProvider> provider
	// 	, Type type
	// );

private:
	const Quality m_quality;
	MediaType m_mediaType;
};
