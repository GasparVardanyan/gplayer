# include "TrackSource.hpp"

TrackSource::TrackSource (Quality type)
	: m_quality (type)
	, m_mediaType (MediaType::Unknown)
{

}

TrackSource::Quality TrackSource::quality () const
{
	return m_quality;
}

TrackSource::MediaType TrackSource::mediaType () const
{
	return m_mediaType;
}
