# pragma once

# include "PlaylistItem.hpp"
# include <QSharedPointer>

# include <QList>

class QString;

class TrackSource;

class Track : public PlaylistItem, public QEnableSharedFromThis <Track>
{
	friend class PlaylistParser;

public:
	enum class SourceType : int {
		Unknown
		, File
		, Youtube
	};

public:
	Track (const QString & source);

	virtual QList <QSharedPointer <Track>> tracks () override final;

	SourceType sourceType ();

protected:
	void setSourceType (SourceType sourceType);

private:
	SourceType m_sourceType;
};
