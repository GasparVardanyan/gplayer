# include "RegularPlaylistProvider.hpp"

# include <QString>
# include <QIODevice>
# include <QFile>

RegularPlaylistProvider::RegularPlaylistProvider (const QString & path)
{
	openDirectory (path);
}

void RegularPlaylistProvider::openDirectory (const QString & path)
{
	m_rootDir = path;
}

bool RegularPlaylistProvider::isReadOnly () const
{
	return false;
}

PlaylistProviderStatus RegularPlaylistProvider::status () const
{

}

QSharedPointer <QIODevice> RegularPlaylistProvider::openFile (const QString & path) const
{
	QFile * file = new QFile (m_rootDir + "/" + path);
	file->open (QIODevice::ReadOnly);

	return QSharedPointer <QFile> (file);
}

bool RegularPlaylistProvider::fileExists (const QString & path) const
{
	return QFile::exists (m_rootDir + "/" + path);
}
