# include "Playlist.hpp"

# include <algorithm>

Playlist::Playlist (QSharedPointer <IPlaylistProvider> provider)
	: m_provider (provider)
	, m_title ("playlist")
	, m_status (PlaylistStatus::NotOpened)
{
}

QSharedPointer <IPlaylistProvider> Playlist::provider ()
{
	return m_provider;
}

QList <QSharedPointer <const PlaylistItem>> Playlist::items () const
{
	QList <QSharedPointer <const PlaylistItem>> ret;

	ret.reserve (m_items.size ());

	std::transform (
		  m_items.begin ()
		, m_items.end ()
		, std::back_inserter (ret)
		, [] (const QSharedPointer <PlaylistItem> & ptr) {
			return QSharedPointer <const PlaylistItem> (ptr);
		}
	);

	return ret;
}

QString Playlist::title () const
{
	return m_title;
}

QSharedPointer <Art> Playlist::art () const
{
	return m_art;
}

void Playlist::clear ()
{
	m_items.clear ();
	m_title = "playlist";
	m_status = PlaylistStatus::NotOpened;
}

void Playlist::setStatus (PlaylistStatus status)
{
	m_status = status;
}

void Playlist::setTitle (const QString & title)
{
	m_title = title;
}

void Playlist::setArt (QSharedPointer <Art> art)
{
	m_art = art;
}

void Playlist::addItem (QSharedPointer <PlaylistItem> item)
{
	m_items.append (item);
}
