QT *= core gui
QT *= widgets

CONFIG *= c++20
DEFINES *= QT_DISABLE_DEPRECATED_BEFORE=0x060000

include (PlaylistManager.pri)
include (Playlist.pri)
include (PlaylistParser.pri)
include (PlaylistProvider.pri)
include (ResourceManager.pri)

HEADERS *= \
    MainWindow.hpp \

SOURCES *= \
    main.cpp \
    MainWindow.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
