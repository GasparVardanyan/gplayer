


# Playlist Skel.
# Only the info used before loading media (audio/video/thumbnail).
# No any media stuff, even path parsing.



HEADERS *= \
    Art.hpp \
    Playlist.hpp \
    PlaylistItem.hpp \
    Group.hpp \
    Track.hpp

SOURCES *= \
    Art.cpp \
    Playlist.cpp \
    PlaylistItem.cpp \
    Group.cpp \
    Track.cpp
