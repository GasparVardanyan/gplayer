# pragma once

# include <QList>
# include <QSharedPointer>

class QString;

class IPlaylistProvider;
class PlaylistItem;
class Art;

enum class PlaylistStatus : int {
	Ready = 0
	, NotOpened = 1
	, NotFound = 2
	, UnknownFileType = 3
	, ParsingError = 4
};

class Playlist : public QEnableSharedFromThis <Playlist>
{
	// TODO: make a copy constructor
	friend class PlaylistParser;

public:
	Playlist (QSharedPointer <IPlaylistProvider> provider);

	QList <QSharedPointer <const PlaylistItem>> items () const;

	QString title () const;
	QSharedPointer <Art> art () const;
	QSharedPointer <IPlaylistProvider> provider ();

	PlaylistStatus status () const;

	void clear ();

protected:
	void setStatus (PlaylistStatus status);
	void setTitle (const QString & title);
	void setArt (QSharedPointer <Art> art);
	void addItem (QSharedPointer <PlaylistItem> item);

private:
	const QSharedPointer <IPlaylistProvider> m_provider;
	PlaylistStatus m_status;
	QString m_title;
	QSharedPointer <Art> m_art;
	QList <QSharedPointer <PlaylistItem>> m_items;
};
