# include "PlaylistItem.hpp"

PlaylistItem::PlaylistItem (Type type, const QString & source)
	: m_type (type)
	, m_source (source)
{

}

PlaylistItem::Type PlaylistItem::type () const
{
	return m_type;
}

QString PlaylistItem::title () const
{
	return m_title;
}

QString PlaylistItem::source () const
{
	return m_source;
}

void PlaylistItem::setTitle (const QString & title)
{
	m_title = title;
}
