# pragma once

# include <QList>
# include <QSharedPointer>

class Track;

class PlaylistItem
{
	friend class PlaylistParser;

public:
	enum class Type : int {
		  Track = 1
		, Group = 2
	};

public:
	PlaylistItem (Type type, const QString & source);

	Type type () const;

	virtual QList <QSharedPointer <Track>> tracks () =0;

	QString title () const;
	QString source () const;

	void setTitle (const QString & title);

private:
	const Type m_type;
	QString m_title;
	QString m_source;
};
