# pragma once

# include <QObject>

# include <QList>

# include <Playlist.hpp>

class PlaylistManager : public QObject
{
	Q_OBJECT
public:
	explicit PlaylistManager (QObject * parent = nullptr);

public:
signals:
	void playlistAdded (Playlist * playlist);
	void prePlaylistRemoved (Playlist * playlist);

private:
	QList <Playlist> m_playlists;
};
