# include "Track.hpp"

Track::Track (const QString & source)
	: PlaylistItem (PlaylistItem::Type::Track, source)
{
}

QList <QSharedPointer <Track>> Track::tracks ()
{
	return {
		sharedFromThis ()
	};
}

Track::SourceType Track::sourceType ()
{
	return m_sourceType;
}

void Track::setSourceType (SourceType sourceType)
{
	m_sourceType = sourceType;
}
