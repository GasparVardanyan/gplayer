# include "PlaylistParser.hpp"

# include <QFile>
# include <QRegularExpression>
# include <QString>
# include <QStringList>
# include <QSharedPointer>
# include <QDebug>

# include "Playlist.hpp"
# include "Art.hpp"
# include "Group.hpp"
# include "Track.hpp"
# include "TrackSource.hpp"
# include "IPlaylistProvider.hpp"

struct PlaylistParser::TrackInfo {
	QString title;
	QString source;
	Track::SourceType sourceType;
};

struct PlaylistParser::GroupInfo {
	QString title;
	QString source;
	QStringList art;
	QList <TrackInfo> tracks;
};

const QRegularExpression PlaylistParser::Matcher_Blank (R"(^\s*$)");
const QRegularExpression PlaylistParser::Matcher_Comment (
	R"(^\s*#\s*(?!EXT(?:M3U\s*$|GRP\s*:|INF\s*:)|PLAYLIST\s*:|:{10}\s*$)(.*)$)"
);

const QRegularExpression PlaylistParser::Matcher_M3U_Header (R"(^\s*#\s*EXTM3U\s*$)");
const QRegularExpression PlaylistParser::Matcher_M3U_Group (R"(^\s*#\s*EXTGRP\s*:(.*)$)");
const QRegularExpression PlaylistParser::Matcher_M3U_Info (R"(^\s*#\s*EXTINF\s*:[^,]*,(.*)$)");
const QRegularExpression PlaylistParser::Matcher_M3U_Playlist_Title (R"(^\s*#\s*PLAYLIST\s*:(.*)$)");
const QRegularExpression PlaylistParser::Matcher_Art_Separator (R"(^\s*#\s*:{10}\s*$)");

const QRegularExpression PlaylistParser::Matcher_File_M3U (R"(^\s*(\w+\.m3u)$)");

const QRegularExpression PlaylistParser::Matcher_URL_Youtube (
	R"(^\s*(?:https?://)?(?:www\.)?youtube\.com/watch\?v=(\S+)\s*$)"
);

void PlaylistParser::Parse (QSharedPointer <Playlist> playlist)
{
	enum class ParsingMode : int {
		  Validation
		, Meta
		, Groups
	};

	enum class ParsingMode_Meta : int {
		  Normal
		, Art
	};

	QSharedPointer <IPlaylistProvider> provider = playlist->provider ();

	QSharedPointer <QIODevice> pl_list = provider->openFile (File_Playlist);

	struct {
		PlaylistStatus status;
		QString platlistTitle;
		QStringList playlistArt;
		QList <GroupInfo> groups;
	} out;

	ParsingMode parsingMode = ParsingMode::Validation;
	ParsingMode_Meta parsingModeMeta = ParsingMode_Meta::Normal;
	out.status = PlaylistStatus::ParsingError;

	while (false == pl_list->atEnd ())
	{
		const QString line = QString::fromUtf8 (pl_list->readLine ());

		if (ParsingMode::Validation == parsingMode)
		{
			if (
				   true == Matcher_Blank.match (line).hasMatch ()
				|| true == Matcher_Comment.match (line).hasMatch ()
			) {
				continue;
			}
			else if (true == Matcher_M3U_Header.match (line).hasMatch ())
			{
				out.status = PlaylistStatus::Ready;
				parsingMode = ParsingMode::Meta;
				parsingModeMeta = ParsingMode_Meta::Normal;
				continue;
			}
			else
			{
				out.status = PlaylistStatus::ParsingError;
				break;
			}
		}

		if (ParsingMode::Meta == parsingMode)
		{
			if (ParsingMode_Meta::Normal == parsingModeMeta)
			{
				if (
					   true == Matcher_Blank.match (line).hasMatch ()
					|| true == Matcher_Comment.match (line).hasMatch ()
				) {
					continue;
				}
				else if (
					  const QRegularExpressionMatch match = Matcher_M3U_Playlist_Title.match (line)
					; true == match.hasMatch ()
				) {
					if (false == out.platlistTitle.isEmpty ())
					{
						out.status = PlaylistStatus::ParsingError;
						break;
					}
					else
					{
						out.platlistTitle = match.captured (1);
						continue;
					}
				}
				else if (true == Matcher_Art_Separator.match (line).hasMatch ())
				{
					if (false == out.playlistArt.isEmpty ())
					{
						out.status = PlaylistStatus::ParsingError;
						break;
					}
					else
					{
						parsingModeMeta = ParsingMode_Meta::Art;
						continue;
					}
				}
				else
				{
					parsingMode = ParsingMode::Groups;
					/* fall through */
				}
			}
			else if (ParsingMode_Meta::Art == parsingModeMeta)
			{
				if (true == Matcher_Comment.match (line).hasMatch ())
				{
					out.playlistArt.append (line);
					continue;
				}
				else if (true == Matcher_Art_Separator.match (line).hasMatch ())
				{
					parsingModeMeta = ParsingMode_Meta::Normal;
					continue;
				}
				else
				{
					out.status = PlaylistStatus::ParsingError;
					break;
				}
			}
		}

		if (ParsingMode::Groups == parsingMode)
		{
			if (
				   true == Matcher_Blank.match (line).hasMatch ()
				|| true == Matcher_Comment.match (line).hasMatch ()
			) {
				continue;
			}
			else if (
				  QRegularExpressionMatch match = Matcher_File_M3U.match (line)
				; true == match.hasMatch ()
			) {
				const QString m3u = match.captured (1);
				out.groups.append (ReadGroup (provider, m3u, out.status)); // Todo: Do MultiThreaded

				if (PlaylistStatus::Ready != out.status)
				{
					break;
				}
			}
			else
			{
				out.status = PlaylistStatus::ParsingError;
				break;
			}
		}
	}

	if (PlaylistStatus::Ready == out.status)
	{
		playlist->setStatus (out.status);
		playlist->setArt (QSharedPointer <Art> ::create (out.playlistArt));

		for (const GroupInfo & g : out.groups)
		{
			QSharedPointer <Group> group = QSharedPointer <Group> ::create (g.source);
			group->setArt (QSharedPointer <Art> ::create (g.art));
			group->setTitle (g.title);

			for (const TrackInfo & t : g.tracks)
			{
				QSharedPointer <Track> track = QSharedPointer <Track> ::create (t.source);
				track->setTitle (t.title);
				track->setSourceType (t.sourceType);

				/*
				QSharedPointer <TrackSource> source =
					QSharedPointer <TrackSource> ::create (TrackSource::Quality::Default)
				;
				source->setProvider (provider);

				if (Track::SourceType::File == t.sourceType)
				{
					source->setSource (t.source);
				}
				else if (Track::SourceType::Youtube == t.sourceType)
				{
					QString trackSourceFile = "";

					for (
						const QString & e :
							  TrackSource::VideoFileExtensions
							+ TrackSource::AudioFileExtensions
					) {
						QString file = t.mediaSource + "." + e;

						if (true == provider->fileExists (file))
						{
							if (false == trackSourceFile.isEmpty ())
							{
								// TODO:
							}
							else
							{
								trackSourceFile = file;
							}
						}
					}

					source->setSource (trackSourceFile);
				}
				*/

				group->addTrack (track);

				qDebug () << group->title () << ":" << track->title ();
			}

			playlist->addItem (group);
		}
	}
	else
	{
		playlist->setStatus (out.status);
	}
}

PlaylistParser::GroupInfo PlaylistParser::ReadGroup (QSharedPointer <IPlaylistProvider> provider, const QString & m3uFile, PlaylistStatus & status)
{
	enum class ParsingMode : int {
		  Meta
		, Tracks
	};

	enum class ParsingMode_Meta : int {
		  Normal
		, Art
	};

	enum class ParsingMode_Track : int {
		  Title
		, File
	};

	QSharedPointer <QIODevice> m3u = provider->openFile (m3uFile);

	GroupInfo out = {
		.source = m3uFile
	};

	ParsingMode parsingMode = ParsingMode::Meta;
	ParsingMode_Meta parsingModeMeta = ParsingMode_Meta::Normal;
	ParsingMode_Track parsingModeTrack = ParsingMode_Track::Title;

	QString currentTrackTitle;

	while (false == m3u->atEnd ())
	{
		const QString line = QString::fromUtf8 (m3u->readLine ());

		if (ParsingMode::Meta == parsingMode)
		{
			if (ParsingMode_Meta::Normal == parsingModeMeta)
			{
				if (
					   true == Matcher_Blank.match (line).hasMatch ()
					|| true == Matcher_Comment.match (line).hasMatch ()
				) {
					continue;
				}
				else if (
					  const QRegularExpressionMatch match = Matcher_M3U_Group.match (line)
					; true == match.hasMatch ()
				) {
					if (false == out.title.isEmpty ())
					{
						status = PlaylistStatus::ParsingError;
						break;
					}
					else
					{
						out.title = match.captured (1);
						continue;
					}
				}
				else if (true == Matcher_Art_Separator.match (line).hasMatch ())
				{
					if (false == out.art.isEmpty ())
					{
						status = PlaylistStatus::ParsingError;
						break;
					}
					else
					{
						parsingModeMeta = ParsingMode_Meta::Art;
						continue;
					}
				}
				else
				{
					parsingMode = ParsingMode::Tracks;
					parsingModeTrack = ParsingMode_Track::Title;
					/* fall through */
				}
			}
			else if (ParsingMode_Meta::Art == parsingModeMeta)
			{
				if (true == Matcher_Comment.match (line).hasMatch ())
				{
					out.art.append (line);
					continue;
				}
				else if (true == Matcher_Art_Separator.match (line).hasMatch ())
				{
					parsingModeMeta = ParsingMode_Meta::Normal;
					continue;
				}
				else
				{
					status = PlaylistStatus::ParsingError;
					break;
				}
			}
		}

		if (ParsingMode::Tracks == parsingMode)
		{
			if (
				   true == Matcher_Blank.match (line).hasMatch ()
				|| true == Matcher_Comment.match (line).hasMatch ()
			) {
				continue;
			}
			else if (
				  const QRegularExpressionMatch match = Matcher_M3U_Info.match (line)
				; true == match.hasMatch ()
			) {
				if (false == currentTrackTitle.isEmpty ())
				{
					status = PlaylistStatus::ParsingError;
					break;
				}
				else
				{
					currentTrackTitle = match.captured (1);
					continue;
				}
			}
			else if (
				  const QRegularExpressionMatch match = Matcher_URL_Youtube.match (line)
				; true == match.hasMatch ()
			) {
				out.tracks.append ({
					  .title = currentTrackTitle
					, .source = line
					, .sourceType = Track::SourceType::Youtube
				});
				currentTrackTitle.clear ();
				continue;
			}
		}
	}

	if (false == currentTrackTitle.isEmpty ())
	{
		status = PlaylistStatus::ParsingError;
	}

	return out;
}
